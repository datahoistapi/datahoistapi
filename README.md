# DATAHOIST API: v1

## root: /dhedgeadmin

### Logged in user [/]

#### Logged in user [GET]

+ Response 200 (application/json)

        [
            {
                "firstName": "Brandan",
                "lastName": "Kelly",
                "entityId": "1",
                "level": ["admin", "vendor", "company", "building"],
                "role": ["full", "read"],
                "userId": "6",
                "phone": "5124738594",
                "email": "jedd@outlook.com",
                "organization": "",
                "AdId": "Jedd@outlook.com"
            }
        ]

### Create Entity [/]

#### Create new entity [POST]
Create a new entity

+ Request (application/json)

        {
            "table": ["User", "Vendor", "Company", "Building", "Group", "Unit", "Tap", "Edge"],
            "Active": 1,
            "companyName": "Test Company",
            "phone": "5129479762",
            "address1": "345 Test Street",
            "city": "Austin",
            "state": "TX",
            "zip": "78757",
            "email": "testy@datahoist.com",
            "contact": "Ray",
            "vendorId": "1"
        }
+ Response 200 (application/json)
        {
            "status":"success",
            "message":"Success!"
        }

### Landing Page Info [/lpinfo/datahoist]

#### Global Landing Page Info [GET]

+ Response 200 (application/json)

        {
            "id": "datahoist",
            "type": "admin",
            "general": "Thanks for using Datahoist IOT",
            "advisory": "It's flooding down in texas and all of the telephone lines are down",
            "marketing": "Datahoist is leading the way in Elevator IOT",
            "PartitionKey": "admin",
            "RowKey": "datahoist",
            "Timestamp": "2017-08-29T19:42:08.448084+00:00",
            "ETag": "W\/\"datetime'2017-08-29T19%3A42%3A08.448084Z'\"",
            "lowTrends": [
                {
                    "unitId": 146,
                    "unitName": "ATIS Unit 1",
                    "buildingName": "ATIS Pilot Building",
                    "buildingId": 70,
                    "companyId": 54
                },
            ]
        }

### Vendor [/vendor/1]

#### List Vendors [GET]

+ Response 200 (application/json)

        [
            {
                "vendorId": 1,
                "vendorName": "Datahoist Direct",
                "address1": "4900 Spicewood Springs Rd.",
                "address2": null,
                "city": "Austin",
                "state": "TX",
                "zip": "78759",
                "country": null,
                "phone": "+15124637682",
                "email": "direct@datahoist.com",
                "website": "http://www.datahoist.com/",
                "description": "Customers that are directly working with Datahoist, including beta tests, etc.",
                "Active": 1,
                "contact": "Dan Bryant"
            },
        ]

### Company [/company:{vendorId}]

#### List Vendor Companies [GET]

+ Response 200 (application/json)

        [
            {
                "companyName": "Datahoist Testing",
                "contact": "Dan Bryant",
                "address1": "4900 Spicewood Springs Rd.",
                "address2": "Suite 117",
                "city": "Austin",
                "state": "TX",
                "country": null,
                "zip": "78759",
                "phone": "5552347534",
                "email": "dan@datahoist.com",
                "companyId": 28,
                "vendorId": 1,
                "Active": 1,
                "companyNotifSlackChans": null
            },
        ]

#### Delete Company [/company/{companyId}] [DELETE]

### Building [/building:{companyId}]

#### List Company Buildings [GET]

+ Response 200 (application/json)

        [
            {
                "buildingName": "Datahoist Demo Building",
                "address1": "4900 Spicewood Springs Rd.",
                "address2": "",
                "city": "Austin",
                "state": "TX",
                "zip": "78759",
                "phone": "5125770519",
                "email": "dbryant@datahoist.com",
                "location": "North East",
                "country": null,
                "buildingId": 51,
                "companyID": 28,
                "companyNotifEmails": null,
                "companyNotifSlackChans": "1",
                "Active": 1,
                "contact": "Dan Bryant"
            }
        ]

#### Delete Building [/building/{buildingId}] [DELETE]

### Group [/group:{buildingId}]

#### List Building Groups [GET]

+ Response 200 (application/json)

        [
            {
                "groupName": "Main",
                "buildingId": "70",
                "groupId": "54",
            },
        ]

#### Delete Group [/group/{groupId}] [DELETE]

### Unit [/unit:{buildingId}]

#### List Building Units [GET]

+ Response 200 (application/json)

        [
            {
                "name": "ATIS Unit 1",
                "elevatorType": "custom",
                "description": "Test elevator",
                "unitId": 146,
                "manufacturer": "Kone",
                "brand": null,
                "capacity": null,
                "doorType": null,
                "doorOperator": "Linear",
                "groupId": 54,
                "Active": 1,
                "groupName": "Main",
                "edgeId": "ad9580c1aede7b95",
                "tapId": "dhCyCr214"
            },
        ]

#### Delete Unit [/unit/{unitId}] [DELETE]

### Edge [/edge]

#### List Edges [GET]

+ Response 200 (application/json)

        [
            {
                "edgeId": "d2f8e9d3e9fab045",
                "unitId": "4",
                "type": "Allwinner2.0",
                "description": "UH-d",
                "BuildingAndUnit": "Datahoist Demo Building, Unit Unit H",
                "simId": "kore 37538",
                "lastMainAppPing": "4/10/2018 10:42:18 AM"
            },
        ]

#### Delete Edge [/edge/{edgeId}] [DELETE]

### Edge without tap [/edgenotap]

#### List edges without tap [GET]

+ Response 200 (application/json)

        [
            {
                "edgeId": "1891b0ff50498499"
            },
        ]

### Tap [/tap]

#### List Taps [GET]

+ Response 200 (application/json)

        [
            {
                "tapId": "dhCyCrP5",
                "tapType": "car tap",
                "edgeId": "ef32ded8364f9ab9",
                "description": "Pilot project, 1.1",
                "pins": "[\r\n  {\"Name\":\"int\", \"Number\":\"5\", \"ActiveHigh\":true},\r\n  {\"Name\":\"ind\", \"Number\":\"6\", \"ActiveHigh\":true},\r\n  {\"Name\":\"fs\",  \"Number\":\"7\", \"ActiveHigh\":true},\r\n  {\"Name\":\"dz\",  \"Number\":\"8\", \"ActiveHigh\":true},\r\n  {\"Name\":\"dcl\", \"Number\":\"4\", \"ActiveHigh\":true},\r\n  {\"Name\":\"dol\", \"Number\":\"3\", \"ActiveHigh\":true},\r\n  {\"Name\":\"ins\", \"Number\":\"2\", \"ActiveHigh\":false},\r\n  {\"Name\":\"pwr\", \"Number\":\"1\", \"ActiveHigh\":true}\r\n ]",
                "boardSerNum": "00022",
                "companyId": null
            },
        ]

#### Delete Tap [/tap/{tapId}] [DELETE]

### Unassigned Tap [/tapua]

#### List Unassigned Taps [GET]

+ Response 200 (application/json)

        [
            {
                "TapId": "TapCy4247"
            },
        ]

## root: /editview

### Edit Entity [/{entityId}]

#### Edit existing entity [PUT]
Update any table record

+ Request (application/json)

        {
            "table": ["User", "Vendor", "Company", "Building", "Group", "Unit", "Tap", "Edge"],
            "phone": "5127380987",
        }

+ Response 200
        "1"

## root: /report

#### CSV building report [POST]

+ Request (application/json)

        {
            bldId: 70,
            report: 'building',
            start: '2018-1-27',
            end: '2018-2-27'
        }

## root: /data/diagnostic

### State [/state/{unitId}]

#### Get unit state [GET]

+ Response 200

{
  "id": "sid000089",
  "StateTime": "2018/04/10 11:43:25.849",
  "carstate": {
    "CarState": "automatic service",
    "StateTime": "2018/04/02 20:18:51.000"
  },
  "doorstate": {
    "DoorState": "door closed",
    "StateTime": "2017/12/20 20:51:31.000",
    "LastSteadyStateTime": "2017/12/20 20:51:31.387"
  },
  "ridepkg": {
    "RunState": "idle",
    "StateTime": "2018/04/10 11:43:25.849",
    "RunStartTime": "2018/04/10 11:43:25.849"
  },
  "servicealert": {
    "Alert": "",
    "StateTime": "2018/04/10 11:43:25.857",
    "ServiceAlert": "We're getting ride data but no door data."
  },
  "datastate": {
    "DataState": " Door Data is over -9 hours old.",
    "StateTime": "2018/04/10 11:43:25.857"
  },
  "lastSystemAppPing": "2018/04/10 11:46:53",
  "lastMainAppPing": "2018/04/10 11:43:25"
}

### Statistics [/stateupdate/{unitId}]

#### Get unit statictics [GET]

+ Response 200

{
  "version": "1.0",
  "info": {
    "id": "sid000089",
    "SessionType": "standard",
    "CompanyID": "ATIS",
    "UnitName": "ATIS Unit 1",
    "UnitId": "146",
    "UnitType": "custom",
    "BuildingName": "ATIS Pilot Building"
  },
  "duty cycle month to date": 4382,
  "duty cycle previous month 1": 17594,
  "duty cycle previous month 2": 14245,
  "duty cycle previous month 3": 15565,
  "beneficial usage month to date": "0.054",
  "beneficial usage previous month 1": "0.999",
  "beneficial usage previous month 2": "1.000",
  "beneficial usage previous month 3": "1.000",
  "Starts month to date": "4452",
  "Starts previous month 1": "17770",
  "Starts previous month 2": "14338",
  "Starts previous month 3": "15669",
  "overall door trend": {
    "doorOverallTrend": "-65",
    "doorOpeningRecentSd": "1.0",
    "doorClosingRecentSd": "1.0",
    "outlier": "1",
    "overallByHour": [
      -100,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64,
      -64
    ]
  },
  "overall ride trend": 0,
  "hourlyDoorUsage": [
    {
      "hour": "2018-04-10T09:54:25",
      "doorCycles": 0
    },
  ],
  "hourlyRideUsage": [
    {
      "hour": "2018-04-10T09:54:25",
      "runs": 0
    },
  ],
  "dailyDoorTrend": [
    {
      "sequenceNumber": 0,
      "sequenceTime": "2018-03-12T11:54:25.8884732Z",
      "displayLabel": null,
      "trendValue": -64,
      "trendData": [
        {
          "reasonString": "Door opening time issue",
          "trendingUp": false,
          "trendContribution": -40
        },
        {
          "reasonString": "Door closing time issue",
          "trendingUp": false,
          "trendContribution": -24
        }
      ]
    },
    {
      "sequenceNumber": 1,
      "sequenceTime": "2018-03-13T11:54:25.8884732Z",
      "displayLabel": null,
      "trendValue": -64,
      "trendData": [
        {
          "reasonString": "Door opening time issue",
          "trendingUp": false,
          "trendContribution": -40
        },
        {
          "reasonString": "Door closing time issue",
          "trendingUp": false,
          "trendContribution": -24
        }
      ]
    },
  ],
  "dailyRideTrend": [
    {
      "sequenceNumber": 0,
      "sequenceTime": "2018-03-12T11:54:25.8884732Z",
      "displayLabel": null,
      "trendValue": -4,
      "trendData": [
        {
          "reasonString": "Accel magnitude changed",
          "trendingUp": false,
          "trendContribution": -4
        }
      ]
    },
    {
      "sequenceNumber": 1,
      "sequenceTime": "2018-03-13T11:54:25.8884732Z",
      "displayLabel": null,
      "trendValue": 0,
      "trendData": []
    },
  ]
}